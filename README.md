## What is this?
This is my final year project ar [**DA-IICT**](https://www.daiict.ac.in/). In this project I worked on developing an automated system to detect bot accounts on Twitter. I developed various models and compared the results with existing models. Existing models are developed by me based on what is mentioned in those works which I will be mentioning below.


## Tools?
Since I am working on Twitter, it makes sense to know my way around in TwitterAPI and how to work on unformatted tweets obtained from there. This lead to development of [**this**](http://tweetdownloader005.herokuapp.com/search) tool that allows one to download tweets in JSON format. For more details about this tool visit https://gitlab.com/devarshirawal0111/tweet-json-downloader. 

![Tweet Downloader](Images/Screenshot from 2020-05-12 11-45-46.png)


## Datasets for classification of accounts
To train and test my models, I am using publicly available datasets obtained from bot repository at **[Botometer](https://botometer.iuni.iu.edu/bot-repository/datasets.html)**. Below is brief description of each dataset I used: 

|Dataset|Description|
|--|--|
|Caverlee 2011  |contains bot account lured by honeypot accounts and verified human accounts  |
|Pronbots 2019|a group of bots that share scam sites|
| Botometer feedback 2019 | manually labeled accounts after receiving feedback from users about Botometer |
| Celebrity 2019 | celebrity account labeled as authentic users |
| Vendor purchased 2019 | Fake followers purchased from several companies |
| Politicalbots 2019 | a group of politically oriented bots |
| Gilani 2017 | annotated bots and human accounts |

Each dataset has only user metadata in the form of Tweet Object. Only information about the user account is available, no tweets or social network data is available. I am unable to provide the actual datasets from the sites as the repo size would have gone over 5GB. Instead the datasets in this repo are preprocessed from those datasets itself by removing all the unnecesarry tweet data. 




## Feature Engineering
![Feature list](Images/features_Scalable.png)

All the features are taken from **user metadata** shown in above figure. Derived features use **user_age**. It is calculated by the difference in **tweet time** and **account creation time**, in terms of hours. **Rate features** show how fast the account is growing.



##  Models and Results
I built a total of 5 supervised machine learning models. They are listed below: ral
| Model | Description |
|--|--|
| Logistic Regression (baseline) | Standard Logistic Regression model with default values |
|  SVM (baseline) | Standard Support Vector Machine with default values |
| Neural Network | 1-layer FeedForward NN |
| CNN | Convolutional Neural Network with 256 filters and a filter size of 5 |
| Stacked NN | 2 layers of CNN+LSTM layers stacked on each other |

The F1-score for **baseline** models ranged from **90%** to **92%**. The results were similar for **Neural Network** models with an F1-score of **96%** to **97%**. The comparative results between all the 5 models is shown below:
 
| Model | F1-Score | AUC Score | Accuracy|
|--|--|--|--|
| Logistic Regression | 90.3% | 75% | 85% |
| SVM | 92% | 87.33% | 87% |
| Neural Network | 96.4% | 95.5% | 94.3% |
| CNN | 96.7% | 96.2% | 94.7% |
| Stacked NN | 97.12% | 96.12% | 95.5% |

Neural Networks are better suited to do this classification. They have relatively higher results compared to other classifiers.


## Comparison with existing works

Some of the other implementations with their approaches, in brief, are as followed:

| Research Paper | Brief description |
|--|--|
| Scalable and Generalizable Bot Detection | I used the same features as them. Their choice for the classifier is Random Forest |
| Botometer | Uses 1209 features and many more datasets including the ones I used. It is also based on Random Forest Classifier |
| Seven Months with the Devils | Uses temporal (time based) features of tweets and user accounts along with social network graph. Uses Random Forest |
| Deep Neural Networks for Bot Detection | Performs account-level classification (similar to mine) and tweet-level classification (uses text in past tweets of users). Implements AdaBoost classifier |



The results of above approaches are taken as they are mentioned in the papers irrespective of dataset as I wasn't able to get their models. This makes this comparison inaccurate, but better than not comparing at all . Comparative results with other implementations are shown below:


| Approach | F1-Score | AUC-Score | Accuracy|
|--|--|--|--|
| Stacked NN | 97.12% | 96.12% | 95.5% |
| 1 | 94% | NA | NA |
| 2 | NA | 97% | NA |
| 3 | 98.6% | 99.5% | 98.62% |
| 4 | 99% | 98.65% | 98.65% |


As seen in the table, Stacked NN is better than 1 and close to 2. However, 3 and 4 are significantly better. As mentioned earlier, the reason can be because of different datasets and feature engineering. 


## Future scope
1. Optimize the Stacked NN model or modify it for better results.
2. Develop a web-app that uses the model to detect bot accounts using the Twitter streaming API.
3. Implementing a web service like API to provide a classification of an account given its userid or username.


## References
1.  Yang, Kai-Cheng, et al. "Scalable and generalizable social bot detection through data selection." arXiv preprint arXiv:1911.09179 (2019).
    
2.  Yang, Kai‐Cheng, et al. "Arming the public with artificial intelligence to counter social bots." Human Behavior and Emerging Technologies 1.1 (2019): 48-61.
    
3.  Lee, Kyumin, Brian David Eoff, and James Caverlee. "Seven months with the devils: A long-term study of content polluters on twitter." Fifth international AAAI conference on weblogs and social media. 2011.
    
4.  Kudugunta, Sneha, and Emilio Ferrara. "Deep neural networks for bot detection." Information Sciences 467 (2018): 312-322.
    
5.  Datasets, 2019, Retrieved May 7, 2020, from [https://botometer.iuni.iu.edu/bot-repository/datasets.html](https://botometer.iuni.iu.edu/bot-repository/datasets.html)
    

6. Tweet Downloader, 2020, Retrieved May 7, 2020, from [http://tweetdownloader005.herokuapp.com/search](http://tweetdownloader005.herokuapp.com/search)
